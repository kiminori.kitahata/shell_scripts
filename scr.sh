#!/bin/sh

: ${SCRIPTDIR:=$HOME/script}
: ${HOST_DIR:=$HOME/script}
: ${SCRIPT_CMD:=script}
: ${SSH_CMD:=/usr/bin/ssh}
: ${VAGRNT_CMD:=vagrant}

SSH_HOST=''
SSH_USER=''

while [ $(expr "$1" : "-.*") -gt 0 ]
do
	case "$1" in
	-s|--ssh)
		# ssh host
		shift
		SSH_HOST="$1"
		SCRIPTDIR="$HOST_DIR/$1"
		;;
	-u|--user)
		# ssh user
		shift
		SSH_USER="$1"
		;;
    -v)
        # VAGRANT_CWD
        shift
        VAGRANT_CWD="$1"
        SCRIPTDIR="$VAGRANT_CWD"
        ;;
	*)
		;;
	esac
	shift
done

if [ -n "$VAGRANT_CWD" -a ! -r "$VAGRANT_CWD/Vagrantfile" ]
then
    echo "$VAGRANT_CWD/Vagrantfile does not exists." 1>&2
    exit 2
fi

if [ -e "$SCRIPTDIR" ]
then
	if [ ! -d "$SCRIPTDIR" ]
	then
		echo "$SCRIPTDIR: not a directory" 1>&2
		exit 1
	fi
else
	if mkdir -p "$SCRIPTDIR"
	then
		: ok
	else
		exit 1
	fi
fi

TIMESTAMP=`date +%y%m%d-%H%M%S`
LOGFILE="$SCRIPTDIR/$TIMESTAMP.log"

if [ -f "$LOGILE" ]
then
	echo "$LOGFILE: already exists" 1>&2
	exit 1
fi

if [ -n "$VAGRANT_CWD" ]
then
    $SCRIPT_CMD "$LOGFILE" env VAGRANT_CWD="$VAGRANT_CWD" $VAGRNT_CMD ssh
    exit 0
fi

if [ -n "$SSH_HOST" ]
then
	if [ -n "$SSH_USER" ]
	then
    	$SCRIPT_CMD "$LOGFILE" $SSH_CMD -l "$SSH_USER" "$SSH_HOST" "$@"
    else
    	$SCRIPT_CMD "$LOGFILE" $SSH_CMD "$SSH_HOST" "$@"
	fi
    exit 0
fi

$SCRIPT_CMD "$LOGFILE" "$@"

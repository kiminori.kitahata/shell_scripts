#!/bin/bash

SCRIPT_NAME=$(basename $0)

## usage
#   print usage of this command.
# @param none
# @stdin none
# @stdout usage text
# @stderr none
# @status always 0
usgae() {
    echo "usage: ${SCRIPT_NAME} [ -d DATE ] [ -p PROJECT ] [ -t TITLE ] [ DIRECTORY ]"
    echo ""
    echo "DATE: date in memo title"
    echo "PROJECT: project name of the memo, appeared in file name."
    echo "TITLE: title of memo following date"
    echo "DIRECTORY: directory of memo file located.  default is current directory."
}

##
# get_ymd_dir STRING
# @param STRING
# @stdin none
# @stdout YM YMD
# @status 2 when the name of operating system is unknown, 0 otherwise.
get_ymd() {
    local system_name=$(uname -s)
    case "$system_name" in
    Darwin)
        if [ $# -gt 0 ]
        then
            date -j "$1" +'%Y-%m %Y-%m-%d'
        else
            date +'%Y-%m %Y-%m-%d'
        fi
        ;;
    Linux)
        if [ $# -gt 0 ]
        then
            date -d "$1" +'%Y-%m %Y-%m-%d'
        else
            date "$1" +'%Y-%m %Y-%m-%d'
        fi
        ;;
    *)
        {
            echo "${system_name}: Unknown system name."
        } 1>&2
        return 2
        ;;
    esac
    return 0
}

# default values of option arguments.
OPT_DATE=""
OPT_PROJ="memo"
OPT_TITLE="memo"
ARG_DIR="."

# parse options.
while [ "$#" -gt 0 -a $(expr "$1" : '-.*') -gt 0 ]
do
    case "$1" in
    -d|--date)
        shift
        OPT_DATE="$1"
        ;;
    -p|--project)
        shift
        OPT_PROJ="$1"
        ;;
    -t|--title)
        shift
        OPT_TITLE="$1"
        ;;
    -h|--help)
        usage
        exit 0
        ;;
    *)
        {
            echo "$1: unknown option"
            usage
        } 1>&2
        exit 2
        ;;
    esac
    shift
done

# main

if [ $# -gt 0 ]
then
    ARG_DIR="$1"
fi

if [ -n "${OPT_DATE}" ]
then
    get_ymd "${OPT_DATE}"
else
    get_ymd
fi | {
    read YM YMD

    mkdir -p "${ARG_DIR}/${YM}"
    echo "# ${YMD} ${OPT_TITLE}" >>"${ARG_DIR}/${YM}/${YMD}_${OPT_PROJ}.md"
}

exit 0
